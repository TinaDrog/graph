import {request} from "./main.service";

const getSubject = (id) => request({
    url: `subject/${id}`,
    method: 'get'
});

const getSubjects = () => request({
    url: `subject`,
    method: 'get'
});

const updateSubject = (id, data = {}) => request({
    url: `subject/${id}`,
    method: 'put',
    data
});

const deleteSubject = (id) => request({
    url: `subject/${id}`,
    method: 'delete'
});

const createSubject = (data) => request({
    url: 'subject',
    method: 'post',
    data
});

export {
    getSubject,
    getSubjects,
    updateSubject,
    deleteSubject,
    createSubject
}