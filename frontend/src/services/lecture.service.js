import {request} from "./main.service";

const getLecture = (id) => request({
    url: `lecture/${id}`,
    method: 'get'
});

const getLectures = () => request({
    url: `lecture`,
    method: 'get'
});

const createLecture = (data) => request({
    url: `lecture`,
    method: 'post',
    data
});

const updateLecture = (id, data = {}) => request({
    url: `lecture/${id}`,
    method: 'post',
    data
});

const deleteLecture = (id) => request({
    url: `lecture/${id}`,
    method: 'delete'
});

export {
    getLecture,
    getLectures,
    createLecture,
    updateLecture,
    deleteLecture
}