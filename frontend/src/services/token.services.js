import {request} from "./main.service";

const getToken = (id) => request({
    url: `token/${id}`,
    method: 'get'
});

const getTokenByUserId = (id) => request({
    url: `token/user/${id}`,
    method: 'get'
});

const getTokens = () => request({
    url: `token`,
    method: 'get'
});

const updateToken = (id, data = {}) => request({
    url: `token/${id}`,
    method: 'put',
    data
});

const deleteToken = (id) => request({
    url: `token/${id}`,
    method: 'delete'
});

export {
    getToken,
    getTokenByUserId,
    getTokens,
    updateToken,
    deleteToken
}