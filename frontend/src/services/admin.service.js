import {request} from "./main.service";

const getCollectionNames = () => request({
    url: 'db/collection-names',
    method: 'get'
});

export {
    getCollectionNames,
}