import {request} from "./main.service";

const getAbility = (id) => request({
    url: `ability/${id}`,
    method: 'get'
});

const getAbilities = () => request({
    url: `ability`,
    method: 'get'
});

const updateAbility = (id, data = {}) => request({
    url: `ability/${id}`,
    method: 'put',
    data
});

const deleteAbility = (id) => request({
    url: `ability/${id}`,
    method: 'delete'
});

const createAbility = (data) => request({
    url: 'ability',
    method: 'post',
    data
});

export {
    getAbility,
    getAbilities,
    updateAbility,
    deleteAbility,
    createAbility
}