import {request} from "./main.service";

const login = (data = {}) => request({
    url: `auth/login`,
    method: 'post',
    data
});

const logout = (data = {}) => request({
    url: `auth/logout`,
    method: 'post',
    data
});

const signUp = (data = {}) => request({
    url: `auth/sign-up`,
    method: 'post',
    data
});

export {
    login,
    logout,
    signUp
}