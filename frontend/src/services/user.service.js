import {request} from "./main.service";

const getUser = (id) => request({
    url: `user/${id}`,
    method: 'get'
});

const getUsers = () => request({
    url: `user`,
    method: 'get'
});

const updateUser = (id, data = {}) => request({
    url: `user/${id}`,
    method: 'put',
    data
});

const deleteUser = (id) => request({
    url: `user/${id}`,
    method: 'delete'
});

const createUser = (data) => request({
    url: 'user',
    method: 'post',
    data
});

export {
    getUser,
    getUsers,
    updateUser,
    deleteUser,
    createUser
}