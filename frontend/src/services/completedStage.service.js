import {request} from "./main.service";

const getCompletedStages = () => request({
    url: `completedStage`,
    method: 'get'
});

const getCompletedStagesByUserId = (id) => request({
    url: `completedStage/${id}`,
    method: 'get'
});

const updateCompletedStages = (data = {}) => request({
    url: `completedStage/update`,
    method: 'put',
    data
});

export {
    getCompletedStages,
    getCompletedStagesByUserId,
    updateCompletedStages
}