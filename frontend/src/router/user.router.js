const userRoutes = [
    {
        path: '/user/:id',
        name: 'UserView',
        component: () => import('../views/User/Edit.vue')
    }, {
        path: '/user/:id/edit',
        name: 'UserEdit',
        component: () => import('../views/User/Edit.vue')
    },
];

export default userRoutes;
