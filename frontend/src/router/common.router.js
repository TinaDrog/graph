import config from '../views/AdminPanel/setup';
const commonRoutes = [];

for (let key of Object.keys(config)) {
    const singular = config[key].singular;
    const capitalize = singular.charAt(0).toUpperCase() + singular.slice(1);

    commonRoutes.push({
        path: `/:caller/${singular}/:id`,
        name: `Admin${capitalize}View`,
        component: () => import(`../views/${capitalize}/View.vue`)
    }, {
        path: `/:caller/${singular}/:id/edit`,
        name: `Admin${capitalize}Edit`,
        component: () => import(`../views/${capitalize}/Edit.vue`)
    },{
        path: `/:caller/${singular}/add`,
        name: `Admin${capitalize}Add`,
        component: () => import(`../views/${capitalize}/Add.vue`)
    });
}

export default commonRoutes;
