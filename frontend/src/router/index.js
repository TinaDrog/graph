import {createRouter, createWebHistory} from "vue-router";
import commonRoutes from "./common.router";
import lectureRoutes from "./lecture.router";
import subjectRoutes from "./subject.router";
import authRoutes from "./auth.router";
import adminRoutes from "./admin.router";
import abilityRoutes from "./ability.router";
import userRoutes from "./user.router";
import tokenRoutes from "./token.router";

const defaultRoutes = [
    {
        path: '/',
        name: 'Home',
        component: () => import('../views/Home'),
    },
];

const routes = defaultRoutes.concat(
    commonRoutes,
    lectureRoutes,
    subjectRoutes,
    authRoutes,
    adminRoutes,
    abilityRoutes,
    userRoutes,
    tokenRoutes
);

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
});

export default router;
