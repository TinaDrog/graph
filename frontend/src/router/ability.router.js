const abilityRoutes = [
    {
        path: '/ability/:id',
        name: 'AbilityView',
        component: () => import('../views/Ability/View.vue')
    }, {
        path: '/ability/:id/edit',
        name: 'AbilityEdit',
        component: () => import('../views/Ability/Edit.vue')
    },
];

export default abilityRoutes;
