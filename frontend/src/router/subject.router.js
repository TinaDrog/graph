const subjectRoutes = [
    {
        path: '/subject/:id',
        name: 'SubjectView',
        component: () => import('../views/Graph.vue')
    }, {
        path: '/subject/:id/edit',
        name: 'SubjectEdit',
        component: () => import('../views/Subject/Edit.vue')
    }
];

export default subjectRoutes;
