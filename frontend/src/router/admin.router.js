const adminRoutes = [
    {
        path: '/admin-panel',
        name: 'AdminPanelView',
        component: () => import('../views/AdminPanel/AdminPanel.vue')
    },
];

export default adminRoutes;
