const lectureRoutes = [
    {
        path: '/lecture/:id/',
        name: 'LectureView',
        component: () => import('../views/Lecture/Lecture.vue')
    }, {
        path: '/lecture/:id/:isCompleted',
        name: 'LectureViewCompleted',
        component: () => import('../views/Lecture/Lecture.vue')
    },{
        path: '/lecture/:id/add',
        name: 'LectureAdd',
        component: () => import('../views/Lecture/Add.vue')
    },{
        path: '/lecture/:rootId/add/root',
        name: 'LectureAddRoot',
        component: () => import('../views/Lecture/Add.vue')
    }, {
        path: '/lecture/:id/edit',
        name: 'LectureEdit',
        component: () => import('../views/Lecture/Edit.vue')
    },
];

export default lectureRoutes;
