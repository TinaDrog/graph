const tokenRoutes = [
    {
        path: '/token/:id',
        name: 'TokenView',
        component: () => import('../views/Graph.vue')
    },
];

export default tokenRoutes;
