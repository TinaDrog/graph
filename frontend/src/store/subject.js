import {getSubject, getSubjects, updateSubject, deleteSubject, createSubject} from "../services/subject.service";
import {getLecture} from "../services/lecture.service";

const mutations = {
    setSubject(state, subject) {
        state.subject = subject;
    },
    setSubjectChild(state, child) {
        state.subjectChild = child;
    },
    setSubjectRoot(state, root) {
        state.subjectRoot = root;
    },
    setSubjects(state, subjects) {
        state.subjects = subjects;
    },
    setCreatedSubject(state, subject) {
        this.createdSubject = subject;
    },
    setFetchSubjectError(state, err) {
        state.fetchSubjectError = err;
    },
    setFetchSubjectChildError(state, err) {
        state.fetchSubjectChildError = err;
    },
    setFetchSubjectRootError(state, err) {
        state.fetchSubjectRootError = err;
    },
    setFetchSubjectsError(state, err) {
        state.fetchSubjectsError = err;
    },
    setUpdateSubjectError(state, err) {
        state.updateSubjectError = err;
    },
    setCreateSubjectError(state, err) {
        state.createSubjectError = err;
    },
    setDeleteSubjectError(state, err) {
        state.deleteSubjectError = err;
    },
};

const actions = {
    async fetchSubject({commit}, id) {
        try {
            const subject = await getSubject(id);
            await commit('setSubject', subject);
            await commit('setSubjectChild', subject);
        } catch (err) {
            commit('setFetchSubjectError', err.message);
        }
    },
    async fetchSubjects({commit}) {
        try {
            const subjects = await getSubjects();
            commit('setSubjects', subjects);
        } catch (err) {
            commit('setFetchSubjectsError', err.message);
        }
    },
    async fetchSubjectChild({commit}, subjectId) {
        try {
            const subject = await getSubject(subjectId);
            commit('setSubjectChild', subject);
        } catch (err) {
            commit('setFetchSubjectChildError', err.message);
        }
    },
    async fetchSubjectRoot({commit}, rootId) {
        try {
            const root = await getLecture(rootId);
            commit('setSubjectRoot', root);
        } catch (err) {
            commit('setFetchSubjectRootError', err.message);
        }
    },
    async createSubject({commit}, payload) {
        try {
            const createdSubject = await createSubject(payload);
            commit('setCreatedSubject', createdSubject);
            commit('setSubject', createdSubject);
        } catch (err) {
            commit('setCreateSubjectError', err.message);
        }
    },
    async updateSubject({commit}, {id, payload}) {
        try {
            await updateSubject(id, payload);
        } catch (err) {
            commit('setUpdateSubjectError', err.message);
        }
    },
    async deleteSubject({commit}, id) {
        try {
            await deleteSubject(id);
            const subjects = await getSubjects();
            commit('setSubjects', subjects);
        } catch (err) {
            commit('setDeleteSubjectError', err.message);
        }
    }
};

const getters = {
    subject: ({subject}) => subject,
    subjects: ({subjects}) => subjects,
    subjectChild: async ({subjectChild}) => {
        const separatedSubjectChild = [];

        if (subjectChild.child && subjectChild.child.length > 0) {
            const rootChildren = {
                ...subjectChild,
                child: await populate(subjectChild.child),
            };
            await separateArrayByLevel(rootChildren.child, 0);
        }

        return separatedSubjectChild;

        async function separateArrayByLevel(child, index) {
            const newChild = [];
            for (let children of child) {
                newChild.push(children);

                if (children.child && children.child.length > 0) {
                    await separateArrayByLevel(children.child, index + 1);
                }
            }
            separatedSubjectChild[index] = newChild;
        }
    },
    subjectRoot: ({subjectRoot}) => subjectRoot,
    createdSubject: ({createdSubject}) => createdSubject,
    fetchSubjectError: ({fetchSubjectError}) => fetchSubjectError,
    fetchSubjectChildError: ({fetchSubjectChildError}) => fetchSubjectChildError,
    fetchSubjectRootError: ({fetchSubjectRootError}) => fetchSubjectRootError,
    fetchSubjectsError: ({fetchSubjectsError}) => fetchSubjectsError,
    updateSubjectError: ({updateSubjectError}) => updateSubjectError,
    createSubjectError: ({createSubjectError}) => createSubjectError,
    deleteSubjectError: ({deleteSubjectError}) => deleteSubjectError,
};

async function populate(child) {
    for (let i = 0; i < child.length; i++) {
        if (child[i].id) {
            child[i] = await getChildren(child[i]);
        }

        if (child[i].child && child[i].child.length > 0) {
            await populate(child[i].child)
        }
    }

    return child;
}

async function getChildren({id, type}) {
    if (type === 'lecture') {
        return {
            ...await getLecture(id),
            type
        };
    }

    return null;
}

const state = () => ({
    subject: {},
    subjects: [],
    subjectChild: [],
    subjectRoot: {},
    createdSubject: {},
    fetchSubjectError: null,
    fetchSubjectChildError: null,
    fetchSubjectRootError: null,
    fetchSubjectsError: null,
    updateSubjectError: null,
    createSubjectError: null,
    deleteSubjectError: null,
});

export default {
    mutations,
    getters,
    actions,
    state
}