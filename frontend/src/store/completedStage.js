import {
    getCompletedStages,
    getCompletedStagesByUserId,
    updateCompletedStages
} from "../services/completedStage.service";

const mutations = {
    setCompletedStages(state, stages) {
        state.completedStages = stages;
    },
    setUserCompletedStages(state, stages) {
        state.userCompletedStages = stages;
    },
    setUpdatedStages(state, stages) {
        state.updatedStages = stages;
    },
    setFetchCompletedStagesError(state, err) {
        state.fetchCompletedStagesError = err;
    },
    setFetchCompletedStagesByIdError(state, err) {
        state.fetchCompletedStagesByIdError = err;
    },
    setUpdateCompletedStagesError(state, err) {
        state.updateCompletedStagesError = err;
    }
};

const actions = {
    async fetchCompletedStages({commit}) {
        try {
            const completedStages = await getCompletedStages();
            commit('setCompletedStages', completedStages);
        } catch (err) {
            commit('setFetchCompletedStagesError', err.message);
        }
    },
    async fetchCompletedStagesByUserId({commit}, userId) {
        try {
            const completedStages = await getCompletedStagesByUserId(userId);
            commit('setUserCompletedStages', completedStages[0]);
        } catch (err) {
            commit('setFetchCompletedStagesByIdError', err.message);
        }
    },
    async updateCompletedStages({commit}, lectureId) {
        try {
            const user = JSON.parse(localStorage.getItem('user'));
            const updatedStages = await updateCompletedStages({
                userId: user._id,
                lectureId
            });

            commit('setUpdatedStages', updatedStages);
        } catch (err) {
            commit('setUpdateCompletedStagesError', err.message);
        }
    }
};

const getters = {
    completedStages: ({completedStages}) => completedStages,
    userCompletedStages: ({userCompletedStages}) => userCompletedStages,
    updatedStages: ({updatedStages}) => updatedStages,
    fetchCompletedStagesError: ({fetchCompletedStagesError}) => fetchCompletedStagesError,
    fetchCompletedStagesByIdError: ({fetchCompletedStagesByIdError}) => fetchCompletedStagesByIdError,
    updateCompletedStagesError: ({updateCompletedStagesError}) => updateCompletedStagesError,
};

const state = () => ({
    completedStages: {},
    userCompletedStages: {},
    updatedStages: {},
    fetchCompletedStagesError: null,
    fetchCompletedStagesByIdError: null,
    updateCompletedStagesError: null
});

export default {
    mutations,
    getters,
    actions,
    state
}