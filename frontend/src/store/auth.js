import {login, logout, signUp} from "../services/auth.service";

const currentUser = JSON.parse(localStorage.getItem('user'));

const mutations = {
    setCurrentUser(state, user) {
        state.currentUser = user;
    },
    setLoginError(state, err) {
        state.loginError = err;
    },
    setStatus(state, status) {
        state.status = status;
    },
    setLogoutSuccess(state, message) {
        state.logoutSuccess = message;
    },
    setLogoutError(state, err) {
        state.logoutError = err;
    },
    setSignUpSuccess(state, message) {
        state.signUpSuccess = message;
    },
    setSignUpError(state, err) {
        state.signUpError = err;
    },
};

const actions = {
    async login({commit}, {email, password}) {
        try {
            const data = await login({email, password});
            localStorage.setItem('user', JSON.stringify(data.userData));
            localStorage.setItem('accessToken', data.accessToken);
            localStorage.setItem('refreshToken', data.refreshToken);

            data.userData.can = function (ability) {
                return this.abilities && this.abilities.indexOf(ability) >= 0;
            };
            commit('setCurrentUser', data.userData);
            commit('setStatus', true);
            commit('setLoginError', null);
        } catch (err) {
            commit('setLoginError', err.message);
        }
    },
    async logout({commit}) {
        try {
            const refreshToken = localStorage.getItem('refreshToken');
            const result = await logout({refreshToken});
            localStorage.removeItem('user');
            localStorage.removeItem('accessToken');
            localStorage.removeItem('refreshToken');

            commit('setCurrentUser', {});
            commit('setStatus', false);
            commit('setLoginError', null);
            commit('setLogoutSuccess', result.message);
        } catch (err) {
            commit('setLogoutError', err.message);
        }
    },
    async signUp({commit}, {nickname, email, password}) {
        try {
            const result = await signUp({nickname, email, password});
            commit('setSignUpSuccess', result.message);
        } catch (err) {
            commit('setSignUpError', err.message);
        }
    }
};

const getters = {
    currentUser: ({currentUser}) => {
        if (currentUser.nickname) {
            currentUser.can = function (ability) {
                return this.abilities && this.abilities.indexOf(ability) >= 0;
            };
        } else {
            currentUser.can = () => false;
        }

        return currentUser
    },
    status: ({status}) => status,
    loginError: ({loginError}) => loginError,
    logoutSuccess: ({logoutSuccess}) => logoutSuccess,
    logoutError: ({logoutError}) => logoutError,
    signUpSuccess: ({signUpSuccess}) => signUpSuccess,
    signUpError: ({signUpError}) => signUpError,
};

const state = () => ({
    currentUser: currentUser ? currentUser : {},
    status: !!currentUser,
    loginError: null,
    logoutSuccess: '',
    logoutError: null,
    signUpSuccess: '',
    signUpError: null,
});

export default {
    mutations,
    getters,
    actions,
    state
}