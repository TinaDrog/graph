import {getCollectionNames} from "../services/admin.service";

const mutations = {
    setCollectionNames(state, names) {
        state.collectionNames = names;
    },
    setFetchCollectionNamesError(state, err) {
        state.fetchCollectionNamesError = err;
    }
};

const actions = {
    async fetchCollectionNames({commit}) {
        try {
            const names = await getCollectionNames();
            commit('setCollectionNames', names);
        } catch (err) {
            commit('setFetchCollectionNamesError', err.message);
        }
    },
};

const getters = {
    collectionNames: ({collectionNames}) => collectionNames,
    fetchCollectionNamesError: ({fetchCollectionNamesError}) => fetchCollectionNamesError
};

const state = () => ({
    collectionNames: [],
    fetchCollectionNamesError: null
});

export default {
    mutations,
    getters,
    actions,
    state
}