import {getToken, getTokenByUserId, getTokens, deleteToken} from "../services/token.services";

const mutations = {
    setToken(state, token) {
        state.token = token;
    },
    setTokens(state, tokens) {
        state.tokens = tokens;
    },
    setFetchTokenError(state, err) {
        state.fetchTokenError = err;
    },
    setFetchTokensError(state, err) {
        state.fetchTokensError = err;
    },
    setDeleteTokenError(state, err) {
        state.deleteTokenError = err;
    }
};

const actions = {
    async fetchToken({commit}, id) {
        try {
            const token = await getToken(id);
            commit('setToken', token);
        } catch (err) {
            commit('setFetchTokenError', err.message);
        }
    },
    async fetchTokenByUserId({commit}, userId) {
        try {
            const token = await getTokenByUserId(userId);
            commit('setToken', token);
        } catch (err) {
            commit('setFetchTokenError', err.message);
        }
    },
    async fetchTokens({commit}) {
        try {
            const tokens = await getTokens();
            commit('setTokens', tokens);
        } catch (err) {
            commit('setFetchTokensError', err.message);
        }
    },
    async deleteToken({commit}, id) {
        try {
            await deleteToken(id);
            const tokens = await getTokens();
            commit('setTokens', tokens);
        } catch (err) {
            commit('setDeleteTokenError', err.message);
        }
    }
};

const getters = {
    token: ({token}) => token,
    tokens: ({tokens}) => tokens,
    fetchTokenError: ({fetchTokenError}) => fetchTokenError,
    fetchTokensError: ({fetchTokensError}) => fetchTokensError,
    deleteTokenError: ({deleteTokenError}) => deleteTokenError,
};

const state = () => ({
    token: {},
    tokens: [],
    fetchTokenError: null,
    fetchTokensError: null,
    deleteTokenError: null,
});

export default {
    mutations,
    getters,
    actions,
    state
}