import {
    getLecture,
    getLectures,
    updateLecture,
    deleteLecture,
    createLecture
} from "../services/lecture.service";

const mutations = {
    setLecture(state, lecture) {
        state.lecture = lecture;
    },
    setLectures(state, lectures) {
        state.lectures = lectures;
    },
    setLectureChild(state, child) {
        state.lectureChild = child;
    },
    setUpdatedLecture(state, lecture) {
        state.updatedLecture = lecture;
    },
    setCreatedLecture(state, lecture) {
        state.createdLecture = lecture;
    },
    setFetchLectureError(state, err) {
        state.fetchLectureError = err;
    },
    setFetchLectureChildError(state, err) {
        state.fetchLectureChildError = err;
    },
    setFetchLecturesError(state, err) {
        state.fetchLecturesError = err;
    },
    setUpdateLectureError(state, err) {
        state.updateLectureError = err;
    },
    setCreateLectureError(state, err) {
        state.createLectureError = err;
    },
    setDeleteLectureError(state, err) {
        state.deleteLectureError = err;
    },
};

const actions = {
    async fetchLecture({commit}, id) {
        try {
            const lecture = await getLecture(id);
            console.log(lecture);
            commit('setLecture', lecture);
        } catch (err) {
            commit('setFetchLectureError', err.message);
        }
    },
    async fetchLectures({commit}) {
        try {
            const lectures = await getLectures();
            commit('setLectures', lectures);
        } catch (err) {
            commit('setFetchLecturesError', err.message);
        }
    },
    async fetchLectureChild({commit}, id) {
        try {
            const lecture = await getLecture(id);
            const child = [];
            for (let children of lecture.child) {
                child.push(await getLecture(children.id));
            }
            commit('setLectureChild', child);
        } catch (err) {
            commit('setFetchLectureChildError', err.message);
        }
    },
    async createLecture({commit}, payload) {
        try {
            const createdLecture = await createLecture(payload);
            commit('setCreatedLecture', createdLecture);
        } catch (err) {
            commit('setCreateLectureError', err.message);
        }
    },
    async updateLecture({commit}, {id, payload}) {
        try {
            const updatedLecture = await updateLecture(id, payload);
            commit('setUpdatedLecture', updatedLecture)
        } catch (err) {
            commit('setUpdateLectureError', err.message);
        }
    },
    async deleteLecture({commit}, id) {
        try {
            await deleteLecture(id);
            const lectures = await getLectures();
            commit('setLectures', lectures);
        } catch (err) {
            commit('setDeleteLectureError', err.message);
        }
    }
};

const getters = {
    lecture: ({lecture}) => lecture,
    lectures: ({lectures}) => lectures,
    lectureChild: ({lectureChild}) => lectureChild,
    createdLecture: ({createdLecture}) => createdLecture,
    updatedLecture: ({updatedLecture}) => updatedLecture,

    fetchLectureError: ({fetchLectureError}) => fetchLectureError,
    fetchLectureChildError: ({fetchLectureChildError}) => fetchLectureChildError,
    fetchLecturesError: ({fetchLecturesError}) => fetchLecturesError,
    updateLectureError: ({updateLectureError}) => updateLectureError,
    createLectureError: ({createLectureError}) => createLectureError,
    deleteLectureError: ({deleteLectureError}) => deleteLectureError,
};

const state = () => ({
    lecture: {},
    lectures: {},
    lectureChild: [],
    createdLecture: {},
    updatedLecture: {},
    fetchLectureError: null,
    fetchLectureChildError: null,
    fetchLecturesError: null,
    updateLectureError: null,
    createLectureError: null,
    deleteLectureError: null,
});

export default {
    mutations,
    getters,
    actions,
    state
}