import {deleteUser, getUser, getUsers, updateUser, createUser} from "../services/user.service";

const mutations = {
    setUser(state, user) {
        state.user = user;
    },
    setUsers(state, users) {
        state.users = users;
    },
    setCreatedUser(state, user) {
        state.createdUser = user;
    },
    setAuthors(state, authors) {
        state.authors = authors;
    },
    setFetchUserError(state, err) {
        state.fetchUserError = err;
    },
    setFetchAuthorsError(state, err) {
        state.fetchAuthorsError = err;
    },
    setFetchUsersError(state, err) {
        state.fetchUsersError = err;
    },
    setUpdateUserError(state, err) {
        state.updateUserError = err;
    },
    setCreateUserError(state, err) {
        state.createUserError = err;
    },
    setDeleteUserError(state, err) {
        state.deleteUserError = err;
    },
};

const actions = {
    async fetchUser({commit}, id) {
        try {
            const user = await getUser(id);
            commit('setUser', user);
        } catch (err) {
            commit('setFetchUserError', err.message);
        }
    },
    async fetchUsers({commit}) {
        try {
            const users = await getUsers();
            commit('setUsers', users);
        } catch (err) {
            commit('setFetchUsersError', err.message);
        }
    },
    async createUser({commit}, payload) {
        try {
            const date = new Date();
            const createdUser = await createUser({
                ...payload,
                registrationDate: `${date.getFullYear()}-${('0' + date.getMonth()).slice(-2)}-${('0' + date.getDate()).slice(-2)}`
            });
            commit('setCreatedUser', createdUser);
        } catch (err) {
            commit('setCreateUserError', err.message);
        }
    },
    async updateUser({commit}, {id, payload}) {
        try {
            await updateUser(id, payload);
        } catch (err) {
            commit('setUpdateUserError', err.message);
        }
    },
    async deleteUser({commit}, id) {
        try {
            await deleteUser(id);
            const users = await getUsers();
            commit('setUsers', users);
        } catch (err) {
            commit('setDeleteUserError', err.message);
        }
    },
    async fetchAuthors({commit}, identifiers) {
        try {
            const authors = [];
            for (let id of identifiers) {
                const author = await getUser(id);
                authors[author._id] = author.nickname;
            }
            commit('setAuthors', authors);
        } catch (err) {
            commit('setFetchAuthorsError', err.message);
        }
    }
};

const getters = {
    user: ({user}) => user,
    users: ({users}) => users,
    createdUser: ({createdUser}) => createdUser,
    authors: ({authors}) => authors,
    fetchUserError: ({fetchUserError}) => fetchUserError,
    fetchAuthorsError: ({fetchAuthorsError}) => fetchAuthorsError,
    fetchUsersError: ({fetchUsersError}) => fetchUsersError,
    updateUserError: ({updateUserError}) => updateUserError,
    createUserError: ({createUserError}) => createUserError,
    deleteUserError: ({deleteUserError}) => deleteUserError,
};

const state = () => ({
    user: {},
    users: [],
    createdUser: {},
    authors: [],
    fetchUserError: null,
    fetchAuthorsError: null,
    fetchUsersError: null,
    updateUserError: null,
    createUserError: null,
    deleteUserError: null,
});

export default {
    mutations,
    getters,
    actions,
    state
}