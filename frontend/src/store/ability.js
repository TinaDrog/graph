import {getAbility, getAbilities, updateAbility, deleteAbility, createAbility} from "../services/ability.service";

const mutations = {
    setAbility(state, ability) {
        state.ability = ability;
    },
    setAbilities(state, abilities) {
        state.abilities = abilities;
    },
    setCreatedAbility(state, ability) {
        state.createdAbility = ability;
    },
    setFetchAbilityError(state, err) {
        state.fetchAbilityError = err;
    },
    setFetchAbilitiesError(state, err) {
        state.fetchAbilitiesError = err;
    },
    setUpdateAbilityError(state, err) {
        state.updateAbilityError = err;
    },
    setCreateAbilityError(state, err) {
        state.createAbilityError = err;
    },
    setDeleteAbilityError(state, err) {
        state.deleteAbilityError = err;
    },
};

const actions = {
    async fetchAbility({commit}, id) {
        try {
            const ability = await getAbility(id);
            commit('setAbility', ability);
        } catch (err) {
            commit('setFetchAbilityError', err.message);
        }
    },
    async fetchAbilities({commit}) {
        try {
            const abilities = await getAbilities();
            commit('setAbilities', abilities);
        } catch (err) {
            commit('setFetchAbilitiesError', err.message);
        }
    },
    async createAbility({commit}, payload) {
        try {
            const createdAbility = await createAbility(payload);
            commit('setCreatedAbility', createdAbility);
        } catch (err) {
            commit('setCreateAbilityError', err.message);
        }
    },
    async updateAbility({commit}, {id, payload}) {
        try {
            await updateAbility(id, payload);
        } catch (err) {
            commit('setUpdateAbilityError', err.message);
        }
    },
    async deleteAbility({commit}, id) {
        try {
            await deleteAbility(id);
            const abilities = await getAbilities();
            commit('setAbilities', abilities);
        } catch (err) {
            commit('setDeleteAbilityError', err.message);
        }
    },
};

const getters = {
    ability: ({ability}) => ability,
    abilities: ({abilities}) => abilities,
    createdAbility: ({createdAbility}) => createdAbility,

    fetchAbilityError: ({fetchAbilityError}) => fetchAbilityError,
    fetchAbilitiesError: ({fetchAbilitiesError}) => fetchAbilitiesError,
    createAbilityError: ({createAbilityError}) => createAbilityError,
    updateAbilityError: ({updateAbilityError}) => updateAbilityError,
    deleteAbilityError: ({deleteAbilityError}) => deleteAbilityError,
};

const state = () => ({
    ability: {},
    abilities: [],
    createdAbility: {},

    fetchAbilityError: null,
    fetchAbilitiesError: null,
    createAbilityError: null,
    updateAbilityError: null,
    deleteAbilityError: null,
});

export default {
    mutations,
    getters,
    actions,
    state
}