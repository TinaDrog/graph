import {createStore} from 'vuex';
import subject from './subject';
import lecture from './lecture';
import auth from './auth';
import user from './user';
import admin from "./admin";
import ability from "./ability";
import token from "./token";
import completedStage from "./completedStage";

export default createStore({
    modules: {
        subject,
        lecture,
        auth,
        user,
        admin,
        ability,
        token,
        completedStage
    }
})
