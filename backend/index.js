const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const http = require('http');
const cors = require('cors');

const config = require('./config');
const mongoHost = config.MONGO_HOST;
const mongoPort = config.MONGO_PORT;
const mongoDbName = config.DATABASE_NAME;

mongoose.connect(
    `mongodb://${mongoHost}:${mongoPort}/${mongoDbName}`,
    {
        useCreateIndex: true, // automatically create database indexes
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false
    }
);

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


const {routes} = require('./src/routes');
routes.map((route) => {
    app.use(`/graph/${route}`, require(`./src/routes/${route}`));
});

const port = config.PORT;
http.createServer({}, app).listen(port);
console.log(`Server is running at ${port}`);