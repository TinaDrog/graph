module.exports = {
    PORT: '3000',
    MONGO_PORT: '27017',
    MONGO_HOST: 'localhost',
    DATABASE_NAME: 'graph',

    ACCESS_TOKEN_LIFE_TIME: 86400,
    ACCESS_TOKEN_SECRET: 'access_token_secret',
    REFRESH_TOKEN_SECRET: 'refresh_token_secret'
};