const router = require('express-promise-router')();
const {token} = require('../controllers');

router.route('/:id').get(token.get);
router.route('/user/:id').get(token.getTokenByUserId);
router.route('/').post(token.create);
router.route('/').get(token.getAll);
router.route('/:id').put(token.update);
router.route('/:id').delete(token.delete);

module.exports = router;