const router = require('express-promise-router')();
const {ability} = require('../controllers');

router.route('/:id').get(ability.get);
router.route('/').post(ability.create);
router.route('/').get(ability.getAll);
router.route('/:id').put(ability.update);
router.route('/:id').delete(ability.delete);

module.exports = router;