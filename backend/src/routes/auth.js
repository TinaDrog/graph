const router = require('express-promise-router')();
const {auth} = require('../controllers');

router.route('/login').post(auth.login);
router.route('/logout').post(auth.logout);
router.route('/sign-up').post(auth.signUp);
router.route('/refresh').post(auth.refreshToken);

module.exports = router;