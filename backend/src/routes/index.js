module.exports = {
    routes: [
        'subject',
        'lecture',
        'auth',
        'user',
        'db',
        'ability',
        'token',
        'completedStage'
    ]
};