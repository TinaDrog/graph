const router = require('express-promise-router')();
const {completedStage} = require('../controllers');

router.route('/').get(completedStage.getCompletedStages);
router.route('/:id').get(completedStage.getCompletedStagesByUserId);
router.route('/update').put(completedStage.updateCompletedStages);

module.exports = router;