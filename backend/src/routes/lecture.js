const router = require('express-promise-router')();
const {lecture} = require('../controllers');

router.route('/:id').get(lecture.get);
router.route('/').post(lecture.create);
router.route('/').get(lecture.getAll);
router.route('/:id').post(lecture.update);
router.route('/:id').delete(lecture.delete);

module.exports = router;