const router = require('express-promise-router')();
const {db} = require('../controllers');

router.route('/collection-names').get(db.getDbCollectionNames);

module.exports = router;