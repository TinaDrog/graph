const router = require('express-promise-router')();
const {subject} = require('../controllers');

router.route('/:id').get(subject.get);
router.route('/').post(subject.create);
router.route('/').get(subject.getAll);
router.route('/:id').put(subject.update);
router.route('/:id').delete(subject.delete);

module.exports = router;