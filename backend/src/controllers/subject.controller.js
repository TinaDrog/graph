const genericCrud = require('./main.controller');
const {Subject} = require('../models');

module.exports = {
    ...genericCrud(Subject),
};