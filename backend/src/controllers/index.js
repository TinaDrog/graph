const subject = require('./subject.controller');
const lecture = require('./lecture.controller');
const auth = require('./auth.controller');
const user = require('./user.controller');
const db = require('./db.controller');
const ability = require('./ability.controller');
const token = require('./token.controller');
const completedStage = require('./completedStage.controller');

module.exports = {
    subject,
    lecture,
    auth,
    user,
    db,
    ability,
    token,
    completedStage
};
