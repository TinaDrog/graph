const genericCrud = (model) => ({
    async get({params: {id}}, res) {
        try {
            const item = await model.findById(id);
            return res.status(200).send(item);
        } catch (err) {
            return res.status(400).send({
                message: `Can't get item`
            });
        }
    },
    async getAll(req, res) {
        try {
            const items = await model.find();
            return res.status(200).send(items);
        } catch (err) {
            return res.status(400).send({
                message: `Can't get items`
            });
        }
    },
    async create({body}, res) {
        try {
            const item = new model(body);
            const newItem = await item.save();
            return res.status(200).send(newItem);
        } catch (err) {
            return res.status(400).send({
                message: `Can't create item`
            });
        }
    },
    async update({params: {id}, body}, res) {
        try {
            const item = await model.findByIdAndUpdate(id, body, {new: true}); // new:true = returns updated element
            return res.status(200).send(item);
        } catch (err) {
            return res.status(400).send({
                message: `Can't update item`
            });
        }
    },
    async delete({params: {id}}, res) {
        try {
            await model.findByIdAndDelete(id);
            return res.status(200).send({status: 'OK', message: 'Deleted'});
        } catch (err) {
            return res.status(400).send({
                message: `Can't delete item`
            });
        }
    }
});

module.exports = genericCrud;
