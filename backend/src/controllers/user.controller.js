const genericCrud = require('./main.controller');
const {User} = require('../models');

module.exports = {
    ...genericCrud(User),
};