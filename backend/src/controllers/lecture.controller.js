const genericCrud = require('./main.controller');
const {Lecture} = require('../models');

module.exports = {
    ...genericCrud(Lecture),
};