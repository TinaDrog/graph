const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const {User, Token, Ability} = require('../models');
const config = require('./../../config');

const accessTokenLifeTime = config.ACCESS_TOKEN_LIFE_TIME;
const accessTokenSecret = config.ACCESS_TOKEN_SECRET;
const refreshTokenSecret = config.REFRESH_TOKEN_SECRET;

module.exports = {
    async login({body: {email, password}}, res) {
        try {
            const user = await User.findOne({email});
            if (!user) {
                return res.status(403).send({
                    message: 'Wrong login or password!'
                });
            } else {
                let isPasswordCorrect = false;
                try {
                    if (await bcrypt.compare(password, user['password'])) {
                        isPasswordCorrect = true;
                    }
                } catch (e) {
                    isPasswordCorrect = false;
                }

                if (isPasswordCorrect) {
                    const accessToken = jwt.sign({
                        userId: user._id,
                        email: user.email,
                    }, accessTokenSecret, {
                        expiresIn: accessTokenLifeTime
                    });
                    const refreshToken = jwt.sign({
                        userId: user._id,
                        email: user.email,
                    }, refreshTokenSecret);
                    const foundToken = await Token.findOne({
                        user: user._id
                    });

                    if (foundToken) {
                        await Token.findByIdAndUpdate(foundToken._id, {token: refreshToken});
                        return res.status(200).send({
                            accessToken,
                            refreshToken,
                            email: user.email
                        });
                    }

                    const item = new Token({
                        token: refreshToken,
                        userId: user._id
                    });
                    await item.save();

                    const abilityItem = await Ability.findOne({role: user._doc.role});

                    const userData = {};
                    for (let key in user._doc) {
                        if (user._doc.hasOwnProperty(key) && key !== 'password') {
                            userData[key] = user._doc[key];
                        }
                    }
                    userData.abilities = abilityItem.abilities;

                    return res.status(200).send({
                        accessToken,
                        refreshToken,
                        userData
                    });
                } else {
                    return res.status(403).send({
                        message: 'Wrong login or password!'
                    });
                }
            }
        } catch (err) {
            return res.status(403).send({
                message: 'Wrong login or password!'
            });
        }
    },
    async logout({body: {refreshToken}}, res) {
        const foundToken = await Token.findOne({token: refreshToken});
        if (!foundToken) {
            return res.status(403).send({
                message: 'User is not authorized!',
            });
        }

        await Token.findByIdAndDelete(foundToken._id);
        return res.status(200).send({
            message: 'User was successfully logout'
        })
    },
    async signUp({body: {nickname, email, password, role = 'student'}}, res) {
        try {
            const userByEmail = await User.findOne({email});
            const userByNick = await User.findOne({nickname});
            if (userByEmail) {
                return res.status(403).send({
                    message: 'This email is not available',
                });
            } else if (userByNick) {
                return res.status(403).send({
                    message: 'This nickname is not available',
                });
            }

            const salt = await bcrypt.genSalt();
            const hashedPassword = await bcrypt.hash(password, salt);
            const createdUser = await new User({
                nickname,
                email,
                password: hashedPassword,
                role
            });
            await createdUser.save();

            return res.status(200).send({
                message: 'User created!',
                newUser: {
                    ...createdUser._doc,
                    password: ''
                }
            });
        } catch (err) {
            return res.status(403).send({
                message: 'Wrong login or password!'
            });
        }
    },
    async refreshToken({body: {refreshToken}}, res) {
        if (!refreshToken) {
            return res.status(403).send({
                message: 'Forbidden (refreshToken)'
            });
        }

        const currentToken = await Token.findOne({token: refreshToken});
        if (!currentToken) {
            return res.status(403).send({
                message: 'Forbidden (currentToken)'
            });
        }

        jwt.verify(refreshToken, refreshTokenSecret, (err, user) => {
            if (err) {
                return res.status(403).send({message: 'Forbidden (verify)'});
            }

            const accessToken = jwt.sign({
                userId: user._id,
                email: user.email,
            }, accessTokenSecret, {
                expiresIn: accessTokenLifeTime
            });

            res.status(200).send({
                accessToken,
                email: user.email
            });
        })
    }
};