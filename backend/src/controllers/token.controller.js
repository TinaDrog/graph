const genericCrud = require('./main.controller');
const {Token} = require('../models');

module.exports = {
    ...genericCrud(Token),
    async getTokenByUserId({params: {id}}, res) {
        try {
            console.log(id);
            const item = await Token.findOne({userId: id});
            return res.status(200).send(item);
        } catch (err) {
            return res.status(400).send({
                message: `Can't get completedStages`
            });
        }
    }
};