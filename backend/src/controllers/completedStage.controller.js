//const genericCrud = require('./main.controller');
const mongoose = require('mongoose');
const {CompletedStage} = require('../models');

module.exports = {
    //...genericCrud(CompletedStage),
    async getCompletedStages(req, res) {
        try {
            const items = await CompletedStage.find();
            return res.status(200).send(items);
        } catch (err) {
            return res.status(400).send({
                message: `Can't get completedStages`
            });
        }
    },
    async getCompletedStagesByUserId({params: {id}}, res) {
        try {
            const item = await CompletedStage.find({userId: id});
            return res.status(200).send(item);
        } catch (err) {
            return res.status(400).send({
                message: `Can't get completedStages`
            });
        }
    },
    async updateCompletedStages({body: {userId, lectureId}}, res) {
        try {
            let item = await CompletedStage.find({userId});
            let updatedItem;
            if (item.length === 0) {
                const newItem = new CompletedStage({
                    userId,
                    lectures: [mongoose.Types.ObjectId(lectureId)],
                    subjects: []
                });
                updatedItem = await newItem.save();
            } else {
                updatedItem = await CompletedStage.updateOne(
                    {
                        userId
                    }, {
                        lectures: [
                            ...item[0].lectures,
                            mongoose.Types.ObjectId(lectureId)
                        ]
                    });
            }

            return res.status(200).send(updatedItem);
        } catch (err) {
            return res.status(400).send({
                message: `Can't update ${model} item`
            });
        }
    }
};