const genericCrud = require('./main.controller');
const {Ability} = require('../models');

module.exports = {
    ...genericCrud(Ability),
};