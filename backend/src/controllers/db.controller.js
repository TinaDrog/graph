const mongoose = require('mongoose');

module.exports = {
    async getDbCollectionNames(req, res) {
        try {
            const names = Object.keys(mongoose.connection.collections);
            const neededNames = names.filter(name => name !== 'completedStages');

            return res.status(200).send(neededNames);
        } catch (e) {
            return res.status(400).send({
                message: "Can't get access to database!"
            });
        }
    }
};