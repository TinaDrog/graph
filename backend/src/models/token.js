const {model, Schema, Schema: {Types: {ObjectId}}} = require('mongoose');

const tokenSchema = new Schema({
    token: {
        type: String,
        default: ''
    },
    userId: {
        type: ObjectId,
        ref: 'users'
    },
});

const Token = model('Token', tokenSchema, 'tokens');

module.exports = Token;