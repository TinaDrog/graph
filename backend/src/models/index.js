const Subject = require('./subject');
const Lecture = require('./lecture');
const User = require('./user');
const Token = require('./token');
const Ability = require('./ability');
const CompletedStage = require('./completedStage');

module.exports = {
    Subject,
    Lecture,
    User,
    Token,
    Ability,
    CompletedStage
};