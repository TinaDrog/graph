const {model, Schema, Schema: {Types: {ObjectId}}} = require('mongoose');

const subjectSchema = new Schema({
    name: {
        type: String,
        default: '',
        maxLength: 100,
        required: true,
    },
    /*parentId: {
        type: ObjectId,
        default: null,
    },*/
    child: [{
        type: {
            type: String,
            enum: ['practice', 'lecture'],
            default: 'lecture',
        },
        id: {
            type: ObjectId,
            default: null,
        },
    }],
    description: {
        type: String,
        default: ''
    },
    author: {
        type: ObjectId,
        default: null
    }
});

const Subject = model('Subject', subjectSchema, 'subjects');

module.exports = Subject;