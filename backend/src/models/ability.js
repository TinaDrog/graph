const {model, Schema} = require('mongoose');

const abilitySchema = new Schema({
    role: {
        type: String,
        //enum: ['student', 'teacher', 'admin'], // refuse to create a new role
        default: 'student'
    },
    abilities: {
        type: Array,
        default: () => []
    }
});

const Ability = model('Ability', abilitySchema, 'abilities');

module.exports = Ability;