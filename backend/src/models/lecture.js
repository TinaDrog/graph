const {model, Schema, Schema: {Types: {ObjectId}}} = require('mongoose');

const lectureSchema = new Schema({
    title: {
        type: String,
        default: '',
        maxLength: 100,
        required: true,
    },
    material: {
        type: String,
        default: '',
    },
    parentId: {
        type: ObjectId,
        default: null,
    },
    /*requirements: [{
        type: {
            type: 'String',
            enum: ['subject', 'lecture', 'practice'],
            default: 'lecture'
        },
        id: {
            type: ObjectId,
            default: null,
        },
    }],*/
    child: [{
        type: {
            type: String,
            enum: ['practice', 'lecture'],
            default: 'lecture',
        },
        id: {
            type: ObjectId,
            default: null,
        },
    }],
});

const Lecture = model('Lecture', lectureSchema, 'lectures');

module.exports = Lecture;