const {model, Schema} = require('mongoose');

const userSchema = new Schema({
    nickname: {
        type: String,
        default: ''
    },
    email: {
        type: String,
        default: ''
    },
    password: {
        type: String,
        default: ''
    },
    role: {
        type: String,
        enum: ['student', 'teacher', 'admin'],
        default: 'student'
    },
    registrationDate: {
        type: Date,
        default: Date.now()
    }
});

const User = model('User', userSchema, 'users');

module.exports = User;