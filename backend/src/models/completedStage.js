const {model, Schema, Schema: {Types: {ObjectId}}} = require('mongoose');

const completedStageSchema = new Schema({
    userId: {
        type: ObjectId,
        default: null,
    },
    subjects: {
        type: Array,
        default: [],
    },
    lectures: {
        type: Array,
        default: [],
    },
});

const CompletedStage = model('CompletedStage', completedStageSchema, 'completedStages');

module.exports = CompletedStage;